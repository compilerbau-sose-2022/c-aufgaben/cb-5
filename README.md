# Übungsblatt 5
## Allgemeine Hinweise
Für diese und alle folgenden Praktikumsaufgaben gilt, dass Einsendungen, die in der jeweils mitgegebenen Testumgebung nicht laufen, mit null Punkten bewertet werden! Das beinhaltet insbesondere alle Programme, die sich nicht fehlerfrei kompilieren lassen. Als Testsystem werden wir dabei gruenau6 mit den dort installierten Compilern und Compilerwerkzeugen benutzen. Prüfen Sie bitte rechtzeitig vor der Abgabe, ob ihr Programm auch dort lauffähig ist. Wir akzeptieren keine Entschuldigungen der Form „aber bei mir Zuhause hat es funktioniert“ ;).

Ebenfalls mit null Punkten werden alle Abgaben bewertet, die sich nicht exakt an die vorgegebenen Formate halten.

> Um Ihnen die Abgabe zu erleichtern, geben wir Ihnen ein Makefile mit, welches die folgenden Ziele unterstützt:
> #### all
> Übersetzt die Quelldateien und erzeugt eine ausführbare Datei.
> #### run
> Übersetzt die Quelldateien, erzeugt eine ausführbare Datei und startet das Testprogramm.
> #### clean
> Entfernt alle Zwischendateien und räumt in Ihrem Verzeichnis auf.
> Bitte achten Sie bei Ihrer Implementation auf Speicherleckfreiheit bei korrekter Benutzung, d.h. bei paarweisem Aufruf von init() und release().

## Abgabemodus
Ihre Lösung ist in einem öffentlich verfügbaren Git-Repository abzugeben.

Zur Lösung der Aufgabe steht für Sie dieses Repository zur Verfügung.
Darin enthalten sind 
- Lexer, 
- Parser, 
- zusätzliche Hilfsstrukturen
  - Symboltabelle 
  - Syntaxbaum 
- sowie einige Testeingabedateien und eine [doxygen-Dokumentation](docs.zip). 
- Die Dokumentation können Sie auch selbst aus den Quellen generieren.

## Aufgabe 1 (100 Punkte)
Implementieren Sie die semantische Analyse in den gegebenen Parser unter Zuhilfenahme der zur Verfügung gestellten Symboltabelle. 
Die Grammatik und die zu prüfende Programmsemantik finden Sie unten. 
Bearbeiten Sie bitte ausschließlich die Datei minako-syntax.y. 
Um Ihnen das Testen zu erleichtern, geben wir Ihnen eine Sammlung von fehlerhaften C1-Programmen mit, die viele (aber nicht alle) semantischen Fehlerquellen abdeckt.

### Semantik
Die Grammatik von C1 finden Sie [online](https://amor.cms.hu-berlin.de/~kunert/lehre/material/c1-grammar.php).

Ein Sichtbarkeitsbereich ist ein Abschnitt des Quellcodes, der zu folgenden Metasymbolen reduziert wird:

- block
- forstatement
- functiondefinition, wobei der Funktionsname Teil des äußeren Sichtbarkeitsbereiches ist

Die Grammatik erlaubt eine Schichtung von Sichtbarkeitsbereichen.
Die äußere Schicht wird als globale Schicht bezeichnet.
C1 unterstützt die implizite Konvertierung von int nach float für Zuweisungen, bei Rückgabewerten und in Ausdrücken.
Ein Typ wird als kompatibel zu einem zweiten Typ bezeichnet, wenn er in den anderen konvertierbar ist.

Folgende semantische Regeln gelten in C1:

- Es muss eine parameterlose main()-Funktion mit dem Rückgabetyp void geben.
- Alle Bezeichner müssen vor ihrer Benutzung deklariert werden.
- Eine Schicht eines Sichtbarkeitsbereiches darf keinen Bezeichner doppelt enthalten.
    - Eine innere Schicht kann einen Bezeichner einer äußeren Schicht jedoch überdecken (aka. shadowing).
- Bei der Namensauflösung von Bezeichnern, werden die Sichtbarkeitsbereiche von innen nach außen durchsucht und der erste Treffer ausgewählt.
- Es gibt keine Variablen des Typs void.
- Ausdrücke des Typs void lassen sich nicht mit printf() ausgeben.
- Die Bedingungen für while, do while, for und if sind boolesche Ausdrücke.
- Funktionsaufrufe sind parameterkonform.
    - Parameter- und Argumentlisten haben identische Länge
    - Parameter- und Argumenttypen sind paarweise identisch (und nicht nur kompatibel!)
- Der Typ des Rückgabewertes ist kompatibel zum Rückgabetyp der aktuell definierten Funktion.
- Zuweisungen erfolgen nur in zuweisungsfähige Strukturen und der Typ der rechten Seite ist kompatibel zum Typ der linken Seite.
- Alle hier nicht betrachteten Fälle werden entsprechend des [C-Standards](https://web.archive.org/web/20181230041359if_/http://www.open-std.org/jtc1/sc22/wg14/www/abq/c17_updated_proposed_fdis.pdf) behandelt.